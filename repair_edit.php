﻿<!DOCTYPE html>
<html >
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=big5">
		<title>維修管理系統</title>    
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/style.css" rel="stylesheet">
		<style>
			p.top {
				font-family : DFKai-sb;
				font-size : 48px;
				font-weight : bold;
			}
		</style>
		<?php 
			session_start();
			include("session.php"); 
			include("sql_connect.php");
		
		?>
		<?php
			$no=$_GET['no'];
			$sql="select *  from misrepair where no='$no'";
			$result= mysql_query($sql);
			$row = @mysql_fetch_row($result);
			$date=$row[1];
			$department=$row[2];
			$name=$row[3];
			$board=$row[4];
			$cpu=$row[5];
			$hd=$row[6];
			$ram=$row[7];
			$power=$row[8];
			$chassis=$row[9];
			$status=$row[10];
			$other=$row[11];	
		?>
	</head>

	<body>
		<div class="jumbotron">
				<div class="container" align="center">
					<p class="top">維修單修改</p>
				</div>
		</div>
	<?php
		$url=base64_encode("/mis/repair.php");
		$check=@session_login('username',"http://localhost/admin/index.php?url=$url",3);
		if($check){
	?>
		<form  name='form'action='repair_edit_finish.php?no=<?php echo $no;?>' enctype='multipart/form-data' method="post" >
			<input type="hidden" name="action" value="edit">
		
			<div class="row">
				<div class="col-md-4 col-xs-4 col-md-offset-4 col-xs-offset-4" >
					<label for="id">日期</label>
					<input type='text' class="form-control" id='date' name='date' value="<?php echo $date;?>">
				</div>
			</div>
			
			</br>
			
			<div class="row">
				<div class="col-md-4 col-xs-4 col-md-offset-4 col-xs-offset-4" >
					<label for="department">單位</label>
					<input type='text' class="form-control" id='department' name='department' value="<?php echo $department;?>" >
				</div>
			</div>
			
			</br>
			<div class="row">
				<div class="col-md-4 col-xs-4 col-md-offset-4 col-xs-offset-4" >
					<label for="name">姓名</label>
					<input type='text' class="form-control" id='name' name='name' value="<?php echo $name;?> ">
				</div>
			</div>
	
			</br>
			<div class="row">
				<div class="col-md-4 col-xs-4 col-md-offset-4 col-xs-offset-4" >
					<label for="board">主機板</label>
					<input type='text' class="form-control" id='board' name='board' value="<?php echo $board;?> ">
				</div>
			</div>
			
			</br>
			<div class="row">
				<div class="col-md-4 col-xs-4 col-md-offset-4 col-xs-offset-4" >
					<label for="cpu">CPU</label>
					<input type='text' class="form-control" id='cpu' name='cpu' value="<?php echo $cpu;?>" >
				</div>
			</div>
			
			</br>
			<div class="row">
				<div class="col-md-4 col-xs-4 col-md-offset-4 col-xs-offset-4" >
					<label for="hd">硬碟</label>
					<input type='text' class="form-control" id='hd' name='hd' value="<?php echo $hd;?>" >
				</div>
			</div>
			
			</br>
			<div class="row">
				<div class="col-md-4 col-xs-4 col-md-offset-4 col-xs-offset-4" >
					<label for="ram">記憶體</label>
					<input type='text' class="form-control" id='ram' name='ram' value="<?php echo $ram;?>" >
				</div>
			</div>
			
			</br>
			<div class="row">
				<div class="col-md-4 col-xs-4 col-md-offset-4 col-xs-offset-4" >
					<label for="power">電源供應器</label>
					<input type='text' class="form-control" id='power' name='power' value="<?php echo $power;?>" >
				</div>
			</div>
			
			</br>
			<div class="row">
				<div class="col-md-4 col-xs-4 col-md-offset-4 col-xs-offset-4" >
					<label for="chassis">機殼</label>
					<input type='text' class="form-control" id='chassis' name='chassis' value="<?php echo $chassis;?>" >
				</div>
			</div>
			
			</br>
			<div class="row">
				<div class="col-md-4 col-xs-4 col-md-offset-4 col-xs-offset-4" >
					<label for="status">狀態</label>
					<input type='text' class="form-control" id='status' name='status' value="<?php echo $status;?>" >
				</div>
			</div>
			
			</br>
			<div class="row">
				<div class="col-md-4 col-xs-4 col-md-offset-4 col-xs-offset-4" >
					<label for="other">備註</label>
					<input type='text' class="form-control" id='other' name='other' value="<?php echo $other;?> ">
				</div>
			</div>
			
			</br>
			<div class="row">
				<div class="col-md-4 col-xs-4 col-md-offset-4 col-xs-offset-4" >
					<input  class="btn btn-primary btn-lg btn-block" type="submit" name="button" id="button" value="修改維修單" >
				</div>
			</div>
		</form>
	<?php
		}
	?>		
		
		<script src="js/bootstrap.min.js"></script>
	</body>
</html>