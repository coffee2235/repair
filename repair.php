﻿<!DOCTYPE html>
<html >
  <head>
    <meta http-equiv="Content-Type" content="text/html;charset=big5">
    <title>維修管理系統</title>    	
	<link href="css/style.css" rel="stylesheet">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	
    <style>
		p.top {
			font-family : DFKai-sb;
			font-size : 48px;
			font-weight : bold;
		}
    </style>
	<?php 
		session_start();
		include("session.php"); 
		include("sql_connect.php");
	?>
    
	<?php	
		$sql="select *  from misrepair where finish=0 order by no";
		$result= mysql_query($sql);
		//資料欄位名稱		
		$search_array=array("日期","單位","姓名","主機板","cpu","硬碟","記憶體","電源供應器","機殼","狀態","備註");
		$search_count=0;
	?>
	
  </head>

  <body>
		<nav class="navbar navbar-inverse navbar-fixed-bottom">
			<div class="navbar-form navbar-left">
				<button type="button" class="btn btn-default" onclick="javascript:location.href='repair_new.php';" >新增</button>
				<button type="button" class="btn btn-default" onclick="javascript:location.href='repair_finish.php';" >完成列表</button>
				<button type="button" class="btn btn-default" onclick="javascript:location.href='repair.php';" >回首頁</button>
			</div>
			<form  action="repair.php" enctype='multipart/form-data' method="post"  class="navbar-form navbar-right" >
				<div class="form-group">
					<select  id="classification" name="classification" class="form-control">
							<option value="not">未指定</option>
						<?php
							while ($search_array[$search_count]!=NULL){
								
						?>
								<option  value="<?php echo $search_count+1?>"><?php echo $search_array[$search_count] ?></option>
						<?php
								$search_count++;
							}
						?>
							
					</select>
					<input type="text" id="search" name="search" class="form-control" placeholder="Search">
				</div>
				<button type="submit" class="btn btn-default">Submiit</button>
			</form>
		</nav>
		<?php 
			$classification=@$_POST['classification'];
			$search=@$_POST['search'];
		?>
	<div class="jumbotron">
		
		<div class="container" align="center">
			<p class="top">機房維修中記錄</p>
		</div>
	</div>
	<?php
		$url=base64_encode("/mis/repair.php");
		$check=@session_login('username',"http://localhost/admin/index.php?url=$url",3);
		if($check){
	?>
	<div class="table-responsive" style="padding:50px;">
		<table class="table table-bordered table table-condensed" >
		
			<thead>
				<tr>
					<th  class="col-md-1 col-xs-1">日期</th>
					<th  class="col-md-1 col-xs-1">單位</th>
					<th  class="col-md-1 col-xs-1">姓名</th>
					<th  class="col-md-1 col-xs-1">主機板</th>
					<th  class="col-md-1 col-xs-1">cpu</th>
					<th  class="col-md-1 col-xs-1">硬碟</th>
					<th  class="col-md-1 col-xs-1">記憶體</th>
					<th  class="col-md-1 col-xs-1">電源供應器</th>
					<th  class="col-md-1 col-xs-1">機殼</th>
					<th  class="col-md-1 col-xs-1">狀態</th>
					<th  class="col-md-1 col-xs-1">備註</th>
					<th  class="col-md-1 col-xs-1">功能</th>

				</tr>
			</thead>
			<tbody>
			<?php		
				while ($row=mysql_fetch_row($result)){
					$no=$row[0];
					$date=$row[1];
					$department=$row[2];
					$name=$row[3];
					$board=$row[4];
					$cpu=$row[5];
					$hd=$row[6];
					$ram=$row[7];
					$power=$row[8];
					$chassis=$row[9];
					$status=$row[10];
					$other=$row[11];
					
					if($search!=NULL and $classification!="not"){
						$serch_datebase=$row[$classification];
						if(preg_match("/$search/i",$serch_datebase)){	
			?>				
							<!--使用搜尋時顯示用-->
							<form action="phoneedit.php" enctype='multipart/form-data' method="post" >
									<tr>
										<td align='center' >
											<input id='date' name='date' type='hidden' value="<?php echo  $date ;?>" >
											<?php echo $date ;?>
										</td>
										<td align='center' >
											<input id='department' name='department' type='hidden' value="<?php echo  $department ;?>" >
											<?php echo $department ;?>
										</td>
										<td align='center' >
											<input id='name' name='name' type='hidden' value="<?php echo  $name ;?>" >
											<?php echo $name ;?>
										</td>
										<td align='center' >
											<input id='board' name='board' type='hidden' value="<?php echo  $board ;?>" >
											<?php echo $board ;?>
										</td>
										<td align='center' >
											<input id='cpu' name='cpu' type='hidden' value="<?php echo  $cpu ;?>" >
											<?php echo $cpu ;?>
										</td>
										<td align='center' >
											<input id='hd' name='hd' type='hidden' value="<?php echo  $hd ;?>" >
											<?php echo $hd ;?>
										</td>
										<td align='center' >
											<input id='ram' name='ram' type='hidden' value="<?php echo  $ram ;?>" >
											<?php echo $ram ;?>
										</td>
										<td align='center' >
											<input id='power' name='power' type='hidden' value="<?php echo  $power ;?>" >
											<?php echo $power ;?>
										</td>
										<td align='center' >
											<input id='chassis' name='chassis' type='hidden' value="<?php echo  $chassis ;?>" >
											<?php echo $chassis ;?>
										</td>
										<td align='center' >
											<input id='status' name='status' type='hidden' value="<?php echo  $status ;?>" >
											<?php echo $status ;?>
										</td>
										<td align='center' >
											<input id='other' name='other' type='hidden' value="<?php echo  $other ;?>" >
											<?php echo $other ;?>
										</td>
										<td align='center'>		
											<button name="reload" type="button" class="btn btn-primary" onclick="javascript:location.href='repair_edit.php?no=<?php echo $no;?>';" >修改紀錄</button>
											<button name="reload" type="button" class="btn btn-warning" onclick="javascript:location.href='repair_button.php?no=<?php echo $no;?> & action=finish ';" >維修完成</button>  					
										</td>
									</tr>
									<tr>
										<td colspan='12'  style="background-color:#dddddd;" >
											&nbsp 
										</td>
									</tr>
								</form>					
		<?php	
						}
					}
					else{
		?>
						<form action="phoneedit.php" enctype='multipart/form-data' method="post" >
							<tr>
								<td align='center' >
									<input id='date' name='date' type='hidden' value="<?php echo  $date ;?>" >
									<?php echo $date ;?>
								</td>
								<td align='center' >
									<input id='department' name='department' type='hidden' value="<?php echo  $department ;?>" >
									<?php echo $department ;?>
								</td>
								<td align='center' >
									<input id='name' name='name' type='hidden' value="<?php echo  $name ;?>" >
									<?php echo $name ;?>
								</td>
								<td align='center' >
									<input id='board' name='board' type='hidden' value="<?php echo  $board ;?>" >
									<?php echo $board ;?>
								</td>
								<td align='center' >
									<input id='cpu' name='cpu' type='hidden' value="<?php echo  $cpu ;?>" >
									<?php echo $cpu ;?>
								</td>
								<td align='center' >
									<input id='hd' name='hd' type='hidden' value="<?php echo  $hd ;?>" >
									<?php echo $hd ;?>
								</td>
								<td align='center' >
									<input id='ram' name='ram' type='hidden' value="<?php echo  $ram ;?>" >
									<?php echo $ram ;?>
								</td>
								<td align='center' >
									<input id='power' name='power' type='hidden' value="<?php echo  $power ;?>" >
									<?php echo $power ;?>
								</td>
								<td align='center' >
									<input id='chassis' name='chassis' type='hidden' value="<?php echo  $chassis ;?>" >
									<?php echo $chassis ;?>
								</td>
								<td align='center' >
									<input id='status' name='status' type='hidden' value="<?php echo  $status ;?>" >
									<?php echo $status ;?>
								</td>
								<td align='center' >
									<input id='other' name='other' type='hidden' value="<?php echo  $other ;?>" >
									<?php echo $other ;?>
								</td>
								<td align='center'>		
									<button name="reload" type="button" class="btn btn-primary" onclick="javascript:location.href='repair_edit.php?no=<?php echo $no;?>';" >修改紀錄</button>
									<button name="reload" type="button" class="btn btn-warning" onclick="javascript:location.href='repair_button.php?no=<?php echo $no;?> & action=finish ';" >維修完成</button>  					
								</td>
							</tr>
							<tr>
								<td colspan='12'  style="background-color:#dddddd;" >
									&nbsp 
								</td>
							</tr>
						</form>
		<?php
					}
				}
		?>		
			
		</table>
	</div>
	<?php
	}
	?>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.min.js"></script>   
  </body>
</html>
